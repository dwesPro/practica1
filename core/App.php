<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 17:30
 */

namespace App\Core;

class App
{
    protected static $elementos = [];

    /**
     * Asigna un nuevo elemento al contenedor DI
     * @param $elemento string Nombre del elemento
     * @param $valor array Datos a almacenar
     */
    public static function asignar($elemento, $valor)
    {
        static::$elementos[$elemento] = $valor;
    }

    /**
     * Devuelve un elemento del contenedor DI
     * @param $elemento string Nombre del elemento
     * @return array
     */
    public static function obtener($elemento)
    {
        if(!array_key_exists($elemento, static::$elementos)) {
            throw new Exception("El elemento {$elemento} no está asignado");
        }
        return static::$elementos[$elemento];
    }
}