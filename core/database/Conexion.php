<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 1:19
 */

class Conexion
{
    /**
     * Realiza la conexión con la BBDD
     * @param $config array Datos de la BBDD
     * @return PDO
     */
    public static function realizar($config)
    {
        try{
            return new PDO(
                $config['conexion'].';dbname='.$config['nombre'],
                $config['usuario'],
                $config['passwd'],
                $config['opciones']
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}