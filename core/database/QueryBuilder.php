<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 1:26
 */

class QueryBuilder
{
    protected $pdo;

    /**
     * QueryBuilder constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Consulta que seleciona todos los datos de una tabla
     * @param $tabla string Tabla a realizar la consulta
     * @return array Datos encontrados
     */
    public function selecionarTodo($tabla)
    {
        $consulta = $this->pdo->prepare("select * from {$tabla}");
        $consulta->execute();
        return $consulta->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta que selecciona todos los datos de una tabla con un limite y orden específicos
     * @param $tabla string Tabla a realizar la consulta
     * @param $cantidad int Cantidad de filas a obtener
     * @param $campo string Columna por la que ordenar los datos
     * @param $orden string Tipo de orden a seguir (ASCENDING, DESCENDING, etc...)
     * @return array Datos encontrados
     */
    public function selecionarOrden($tabla, $cantidad, $campo, $orden)
    {
        $consulta = $this->pdo->prepare("select * from {$tabla} ORDER BY {$campo} {$orden} LIMIT {$cantidad}");
        $consulta->execute();
        return $consulta->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta que devuelve todos los datos de una tabla con un límite y búsquedas específicas
     * @param $tabla string Tabla a realizar la consulta
     * @param $limite int Cantidad de filas a obtener
     * @param $offset int Desde donde comenzar a contar
     * @param $busqueda string Búsqueda a realizar
     * @return array Datos encontrados
     */
    public function selecionarTodoLimitado($tabla, $limite, $offset, $busqueda)
    {
        $query = "select SQL_CALC_FOUND_ROWS * from {$tabla}";
        if (strlen($busqueda) > 0) {
            $query .= " WHERE {$busqueda}";
        }
        $query .= " LIMIT {$limite} OFFSET {$offset};";
        $consulta = $this->pdo->prepare(" {$query} SELECT FOUND_ROWS() as total;");
        $consulta->execute();
        $datos = $consulta->fetchAll(PDO::FETCH_CLASS);
        $consulta->nextRowset();
        $total = $consulta->fetchColumn();
        return [
            "datos" => $datos,
            "total" => ceil($total / $limite)
        ];
    }

    /**
     * Consulta que selecciona una fila concreta de una tabla especificada
     * @param $tabla string Tabla a realizar la consulta
     * @param $datos string Datos por lo que limitar la búsqueda
     * @return array Datos encontrados
     */
    public function seleccionar($tabla, $datos)
    {
        $sql = sprintf(
            'select * from %s where (%s)',
            $tabla,
            $datos
        );

        try{
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_CLASS);
        }catch (PDOException $e) {
            die($e);
        }
    }

    /**
     * Consulta que agrega datos a una tabla
     * @param $tabla string Tabla a realizar la consulta
     * @param $datos string Datos a insertar
     * @return void
     */
    public function insertar($tabla, $datos)
    {
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $tabla,
            implode(', ', array_keys($datos)),
            ':' . implode(', :', array_keys($datos))
        );

        try{
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute($datos);
        }catch (PDOException $e) {
            die($e);
        }
    }

    /**
     * Consulta que elimina una fila de una tabla
     * @param $tabla string Tabla a realizar la consulta
     * @param $dato string Datos por lo que limitar la búsqueda
     * @return void
     */
    public function eliminar($tabla, $dato)
    {
        $sql = sprintf(
            'delete from %s where (%s)',
            $tabla,
            $dato
        );

        try{
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute();
        }catch (PDOException $e) {
            die($e);
        }
    }

    /**
     * Consulta que actualiza una fila de una tabla
     * @param $tabla string Tabla a realizar la consulta
     * @param $id int ID de la fila
     * @param $datos string Datos a actualizar
     * @return void
     */
    public function actualizar($tabla, $id, $datos)
    {
//        UPDATE usuarios SET apellido1='prueba1', apellido2='prueba2' WHERE id=1;
        $sql = sprintf(
            'update %s set %s where %s',
            $tabla,
            $datos,
            $id
        );

        try{
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute();
        }catch (PDOException $e) {
            die($e);
        }
    }
}