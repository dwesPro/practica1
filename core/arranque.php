<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 1:39
 */

use App\Core\App;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

App::asignar('config', require 'config.php');

App::asignar('database', new QueryBuilder(
    Conexion::realizar(App::obtener('config')['database'])
));


$loader = new Twig_Loader_Filesystem('app/views');
$twig = new Twig_Environment($loader);

/**
 * Muestra una vista
 * @param $nombre string Nombre de la vista
 * @param array Datos necesarios por la vista
 * @throws Twig_Error_Loader
 * @throws Twig_Error_Runtime
 * @throws Twig_Error_Syntax
 */
function view($nombre, $datos = [])
{
    global $twig;
    $arr = $datos;
    $template = $twig->load("{$nombre}.view.twig");
    if(isset($_SESSION['usuario'])) {
        $arr['sesion'] = $_SESSION['usuario'];
    }
    echo $template->render($arr);
}

/**
 * Redirige a una ruta
 * @param $ruta string Ruta a la que redirigir
 */
function redirigir($ruta)
{
    header("Location: /{$ruta}");
}

/**
 * Genera un archivo PDF
 * @param $arr array Datos con los que generar el PDF
 * @param $titulo string Titulo de la página
 * @param $nombreFichero string Nombre del fichero
 * @return void
 */
function escribePDF($arr, $titulo, $nombreFichero)
{
    global $html2pdf;
    $datos = $arr;
    try {
        // Obtenemos el codigo html
        ob_start();
        require 'app/views/pdf/plantilla.php';
        $contenido = ob_get_clean();

        $html2pdf = new Html2Pdf('P', 'A4', 'es');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($contenido);
        $html2pdf->output($nombreFichero . '.pdf', 'D');
    } catch (Html2PdfException $e) {
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }
}