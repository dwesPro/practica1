<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 3:39
 */

namespace App\Core;

class Solicitud
{
    /**
     * Devuelve la ruta en la que nos encontramos
     * @return string Ruta normalizada
     */
    public static function uri()
    {
        return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    }

    /**
     * Devuelve el método por el que accedemos
     * @return string Método (GET o POST)
     */
    public static function metodo()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}