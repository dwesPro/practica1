<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 2:47
 */

namespace App\Core;

class Router
{
    protected $rutas = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Carga las rutas en el router
     * @param $archivo string Fichero con las rutas
     * @return static Router
     */
    public static function cargar($archivo)
    {
        $router = new static();
        require $archivo;
        return $router;
    }

    /**
     * Agrega una ruta GET
     * @param $uri string Ruta
     * @param $controlador string Controlador al que llamar
     */
    public function get($uri, $controlador)
    {
        $this->rutas['GET'][$uri] = $controlador;
    }

    /**
     * Agrega una ruta POST
     * @param $uri string Ruta
     * @param $controlador string Controlador al que llamar
     */
    public function post($uri, $controlador)
    {
        $this->rutas['POST'][$uri] = $controlador;
    }

    /**
     * Llama al controlador especificado
     * @param $uri string Ruta
     * @param $metodo string Método (GET o POST)
     * @return mixed Datos del controlador
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function dirigir($uri, $metodo)
    {
        if(array_key_exists($uri, $this->rutas[$metodo])){
            $datos = explode('@', $this->rutas[$metodo][$uri]);
            $controlador = $datos[0];
            $accion = $datos[1];
            return $this->llamarAccion($controlador, $accion);
        }
        if($uri == "install") {
            return require 'install/index.php';
        }
        view('error');
    }

    /**
     * Llama a una función de un controlador
     * @param $controlador string Controlador
     * @param $accion string Nombre de la función
     * @return mixed Datos de la función
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function llamarAccion($controlador, $accion)
    {
        $controlador = "App\\Controllers\\{$controlador}";
        $controlador = new $controlador;

        if(!method_exists($controlador, $accion)) {
            view('error');
        }

        return $controlador->$accion();
    }
}