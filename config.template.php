<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 1:48
 */

return [
  'database' => [
      'nombre' => '', // Nombre de la base de datos.
      'usuario' => '', //Usuario de la base de datos.
      'passwd' => '', //Contraseña de la base de datos.
      'conexion' => '', //Conexión de la BBDD. Ej: 'mysql:host=127.0.0.1'.
      'opciones' => [] //Otras opciones de PDO.
  ]
];