<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 09/12/2017
 * Hora: 19:05
 */

if(isset($_POST['datos'])) {
    $_POST['datos']['conexion'] = 'mysql:host=' . $_POST['datos']['conexion'];
    creaConfig($_POST['datos']);
    if(!compruebaEscritura($_POST['datos'])) {
        $error = true;
        require 'vistaForm.php';
    }
    if(!creaBBDD()) {
        $error = true;
        require 'vistaForm.php';
    } else {
        $creado = true;
        require 'vistaForm.php';
    }
} else {
    require 'vistaForm.php';
}

/**
 * Genera el archivo de configuración
 * @param $datos array Datos de la BBDD
 * @return void
 */
function creaConfig($datos){
    $contenido = "<?php
    
return [
  'database' => [
      'nombre' => '{$datos['nombre']}', // Nombre de la base de datos.
      'usuario' => '{$datos['usuario']}', //Usuario de la base de datos.
      'passwd' => '{$datos['passwd']}', //Contraseña de la base de datos.
      'conexion' => '{$datos['conexion']}', //Conexión de la BBDD. Ej: 'mysql:host=127.0.0.1'.
      'opciones' => [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::MYSQL_ATTR_INIT_COMMAND => \"SET NAMES 'UTF8'\"
      ] //Otras opciones de PDO.
  ]
];
    ";
    $fichero = fopen($_SERVER['DOCUMENT_ROOT'] . "/config.php","wb");
    fwrite($fichero, $contenido);
    fclose($fichero);
}

/**
 * Comprueba que los datos insertados y los del archivo generado concuerden
 * @param $datos array Datos a comprobar
 * @return bool
 */
function compruebaEscritura($datos) {
    $config = require $_SERVER['DOCUMENT_ROOT'] . "/config.php";
    $config = $config['database'];
    unset($config['opciones']);
    return count(array_diff($config,$datos)) == 0;
}

/**
 * Consulta que crea la BBDD si no existe
 * @return bool Estado del la consulta
 */
function creaBBDD(){
    $config = require $_SERVER['DOCUMENT_ROOT'] . "/config.php";
    $config = $config['database'];
    try {
        $pdo = new PDO($config['conexion'], $config['usuario'], $config['passwd']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "CREATE DATABASE IF NOT EXISTS {$config['nombre']} DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
        $pdo->exec($query);
        $query = "USE {$config['nombre']}";
        $pdo->exec($query);
        $query = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/install/sql.sql");
        $pdo->exec($query);
        return true;
    } catch (PDOException $e) {
        return false;
    }
}