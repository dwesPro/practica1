-- --------------------------------------------------------
-- Host:                         192.168.10.10
-- Versión del servidor:         10.2.10-MariaDB-10.2.10+maria~xenial-log - mariadb.org binary distribution
-- SO del servidor:              debian-linux-gnu
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla practica.anuncios
CREATE TABLE IF NOT EXISTS `anuncios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `precio` decimal(6,2) NOT NULL DEFAULT 0.00,
  `precioVenta` decimal(6,2) DEFAULT 0.00,
  `estado` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `porcentaje` decimal(5,2) NOT NULL DEFAULT 0.00,
  `categoria` int(2) NOT NULL DEFAULT 0,
  `datos` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Usuario` (`usuario`),
  CONSTRAINT `FK_Usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla practica.anuncios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `anuncios` DISABLE KEYS */;
INSERT INTO `anuncios` (`id`, `titulo`, `precio`, `precioVenta`, `estado`, `porcentaje`, `categoria`, `datos`, `fecha`, `usuario`) VALUES
	(19, 'Reloj hombre', 85.00, 119.00, '0', 40.00, 3, 'Reloj para hombre', '2017-12-09 21:53:08', 20),
	(20, 'Portatil HP', 350.00, 402.50, '1', 15.00, 1, 'Portatil HP con Windows 10 usado', '2017-12-09 21:59:56', 24),
	(21, 'Bicicleta', 400.00, 500.00, '1', 25.00, 4, 'Bicicleta poco usada', '2017-12-09 22:03:35', 21),
	(22, 'Citroën DS3', 4000.00, 6280.00, '2', 57.00, 1, 'Citroën DS3 con espejo retrovisor izquierdo roto', '2017-12-09 22:07:47', 22),
	(23, 'OPM - Tomo 01', 4.00, 8.00, '0', 100.00, 2, 'Saitama es un poderoso superhéroe calvo que derrota fácilmente a los enemigos con un único golpe en la Ciudad Z de Japón. Por ello, Saitama en todo momento está buscando contrincantes.', '2017-12-09 22:12:53', 19),
	(24, 'GPS TomTom', 80.00, 116.80, '2', 46.00, 6, 'GPS TomTom con altavoz estropeado', '2017-12-09 22:27:07', 18);
/*!40000 ALTER TABLE `anuncios` ENABLE KEYS */;

-- Volcando estructura para tabla practica.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `cod` int(2) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla practica.categorias: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`cod`, `nombre`) VALUES
	(1, 'Informatica'),
	(2, 'Libros'),
	(3, 'Moda'),
	(4, 'Deportes y aire libre'),
	(5, 'Vehículos'),
	(6, 'Electrónica');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla practica.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fichero` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anuncio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla practica.fotos: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` (`id`, `fichero`, `anuncio`) VALUES
	(21, 'phpLclYnN.jpg', 19),
	(22, 'phpKjq6oK.jpg', 19),
	(23, 'phpMireNq.jpg', 20),
	(24, 'phpARVd3A.jpg', 20),
	(25, 'php16bjt0.jpg', 21),
	(26, 'phppOXYxp.jpg', 21),
	(27, 'phpKEnp0U.jpg', 22),
	(28, 'php8SxmsZ.jpg', 22),
	(29, 'php0qLqWp.jpg', 23),
	(30, 'phpPieVkq.jpg', 23),
	(31, 'php2urjn6.jpg', 24),
	(32, 'phpdKzF3b.jpg', 24);
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;

-- Volcando estructura para tabla practica.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `tipo` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla practica.logs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Volcando estructura para tabla practica.provincias
CREATE TABLE IF NOT EXISTS `provincias` (
  `cod` int(2) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla practica.provincias: ~52 rows (aproximadamente)
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` (`cod`, `nombre`) VALUES
	(1, 'Alava'),
	(2, 'Albacete'),
	(3, 'Alicante'),
	(4, 'Almería'),
	(5, 'Ávila'),
	(6, 'Badajoz'),
	(7, 'Islas Baleares'),
	(8, 'Barcelona'),
	(9, 'Burgos'),
	(10, 'Cáceres'),
	(11, 'Cádiz'),
	(12, 'Castellón'),
	(13, 'Ciudad Real'),
	(14, 'Córdoba'),
	(15, 'A Coruña'),
	(16, 'Cuenca'),
	(17, 'Girona'),
	(18, 'Granada'),
	(19, 'Guadalajara'),
	(20, 'Guipzcoa'),
	(21, 'Huelva'),
	(22, 'Huesca'),
	(23, 'Jaén'),
	(24, 'León'),
	(25, 'Lleida'),
	(26, 'La Rioja'),
	(27, 'Lugo'),
	(28, 'Madrid'),
	(29, 'Málaga'),
	(30, 'Murcia'),
	(31, 'Navarra'),
	(32, 'Ourense'),
	(33, 'Asturias'),
	(34, 'Palencia'),
	(35, 'Las Palmas'),
	(36, 'Pontevedra'),
	(37, 'Salamanca'),
	(38, 'S. Cruz de Tenerife'),
	(39, 'Cantabria'),
	(40, 'Segovia'),
	(41, 'Sevilla'),
	(42, 'Soria'),
	(43, 'Tarragona'),
	(44, 'Teruel'),
	(45, 'Toledo'),
	(46, 'Valencia'),
	(47, 'Valladolid'),
	(48, 'Vizcaya'),
	(49, 'Zamora'),
	(50, 'Zaragoza'),
	(51, 'Ceuta'),
	(52, 'Melilla');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;

-- Volcando estructura para procedimiento practica.registraLog
CREATE DEFINER=`homestead`@`%` PROCEDURE `registraLog`(
	IN `pUsuario` INT,
	IN `pTipo` VARCHAR(50),
	IN `pTabla` VARCHAR(50)


)
BEGIN
DECLARE pRol TINYINT(1);
DECLARE pRolTxt VARCHAR(50);
SET pRol = (SELECT admin FROM usuarios WHERE id=pUsuario);
IF(pRol = 0) THEN
	SET pRolTxt = 'Anunciante';
ELSE
	SET pRolTxt = 'Administrador';
END IF;
INSERT INTO logs(usuario, rol, tipo, tabla) VALUES (pUsuario, pRolTxt, pTipo, pTabla);
END;

-- Volcando estructura para tabla practica.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nif` varchar(9) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nombre` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellido1` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellido2` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `foto` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `usuario` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `passwd` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `direccion` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `poblacion` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `codigop` int(5) NOT NULL DEFAULT 0,
  `provincia` int(2) NOT NULL DEFAULT 0,
  `telf` int(9) NOT NULL DEFAULT 0,
  `correo` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `web` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `blog` varchar(40) COLLATE utf8_unicode_ci DEFAULT '0',
  `twitter` varchar(40) COLLATE utf8_unicode_ci DEFAULT '0',
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `nif` (`nif`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla practica.usuarios: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nif`, `nombre`, `apellido1`, `apellido2`, `foto`, `usuario`, `passwd`, `direccion`, `poblacion`, `codigop`, `provincia`, `telf`, `correo`, `web`, `blog`, `twitter`, `fecha`, `admin`) VALUES
	(17, '12345678A', 'admin', 'admin', 'admin', 'usuario.jpg', 'admin', 'admin', 'admin', 'admin', 22000, 1, 959123456, 'admin@admin.com', 'https://www.google.es', '', '', '2017-12-09 20:57:54', 1),
	(18, '56745678A', 'Cristian', 'Martínez', 'Fernández', 'phpthRAj6.png', 'cris25', 'cris25', 'C/Madrid', 'Madrid', 29000, 28, 959768594, 'cris25@hotmail.com', 'https://www.google.es', 'https://www.cris25.wordpress.es', 'https://twitter.com/cris25', '2017-12-09 21:08:04', 0),
	(19, '62345432A', 'Alberto', 'Fernández', 'López', 'phpX79Dzd.png', 'albertST', 'albertST', 'C/Sevilla', 'Huelva', 22000, 21, 959765594, 'albertST@gmail.com', 'https://www.reddit.com', 'https://www.albertST.wordpress.es', 'https://twitter.com/albertST', '2017-12-09 21:30:03', 0),
	(20, '42335678A', 'Carlos', 'Hernández', 'Díaz', 'phptWzZG0.png', 'carlos44', 'carlos44', 'C/ Algarve', 'Jerez de la Frontera', 26758, 21, 959738219, 'carlos44@yahoo.es', 'https://www.web.com', 'https://www.carlos44.blogspot.es', '', '2017-12-09 21:34:03', 0),
	(21, '52345478A', 'Andrea', 'Gutiérrez', 'Navarro', 'phplyWqh1.png', 'rita23', 'rita23', 'C/Bécquer', 'Sevilla', 23000, 41, 687463859, 'rita23@hotmail.com', 'https://www.rita23.me', 'https://www.rita23.wordpress.es', 'https://twitter.com/rita23', '2017-12-09 21:37:42', 0),
	(22, '52345654G', 'Carla', 'Torres', 'Vázquez', 'phpw3JSmQ.png', 'carlaMS', 'carlaMS', 'C/El Coso', 'Huesca', 27564, 1, 687463345, 'carlaMS@gmail.es', 'https://www.elpais.com', '', '', '2017-12-09 21:41:27', 0),
	(23, '62345542H', 'Sandra', 'Rodríguez', 'Flores', 'phpUhtgxg.png', 'sandraRod', 'sandraRod', 'C/Alfonso I', 'Zaragoza', 26748, 50, 675849234, 'sandraRod@hotmail.es', 'https://www.sandraRod.com', '', 'https://twitter.com/sandraRod', '2017-12-09 21:44:15', 0),
	(24, '47545478H', 'Raquel', 'García', 'Sánchez', 'php78bI5k.png', 'raquel85', 'raquel85', 'C/Mayor', 'Alcalá de Henares', 21456, 28, 959475869, 'raquel85@gmail.es', 'https://www.raquelModa.es', 'https://www.raquelModa.wordpress.es', 'https://twitter.com/raquel85', '2017-12-09 21:48:27', 0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Volcando estructura para disparador practica.anuncios_before_insert
CREATE TRIGGER `anuncios_before_insert` BEFORE INSERT ON `anuncios` 
FOR EACH ROW SET NEW.precioVenta = NEW.precio + (NEW.precio * NEW.porcentaje * 0.01);

-- Volcando estructura para disparador practica.anuncios_before_update
CREATE TRIGGER `anuncios_before_update` BEFORE UPDATE ON `anuncios` 
FOR EACH ROW SET NEW.precioVenta = NEW.precio + (NEW.precio * NEW.porcentaje * 0.01);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
