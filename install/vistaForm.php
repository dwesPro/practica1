<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instalador</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php if (isset($error)) : ?>
        <div class="row mt-3">
        <div class="col-sm-5 mx-auto">
            <div class="alert alert-danger">
                Ha ocurrido un error
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if (isset($creado)) : ?>
        <div class="row mt-3">
            <div class="col-sm-5 mx-auto">
                <div class="alert alert-success">
                    ¡La BBDD ha sido creada!
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row mt-<?= isset($error) ? '2' : '5' ?>">
        <div class="col-sm-7 mx-auto">
            <form method="post">
                <div class="form-group row justify-content-center align-items-center">
                    <label for="lBBDD" class="col-sm-3 col-form-label">Base de datos</label>
                    <div class="col-sm-6">
                        <input type="text" name="datos[nombre]" class="form-control" id="lBBDD" placeholder="Base de datos">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <label for="lUsuario" class="col-sm-3 col-form-label">Usuario</label>
                    <div class="col-sm-6">
                        <input type="text" name="datos[usuario]" class="form-control" id="lUsuario" placeholder="Usuario">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <label for="lPasswd" class="col-sm-3 col-form-label">Contraseña</label>
                    <div class="col-sm-6">
                        <input type="password" name="datos[passwd]" class="form-control" id="lPasswd" placeholder="Contraseña">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <label for="lServidor" class="col-sm-3 col-form-label">Servidor</label>
                    <div class="col-sm-6">
                        <input type="text" name="datos[conexion]" class="form-control" id="lServidor" placeholder="Servidor" value="127.0.0.1">
                    </div>
                </div>
                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>