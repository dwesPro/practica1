<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 23:40
 */

namespace App\Models;
use \App\Core\App;

class Anuncio
{
    /**
     * Consulta que devuelve una cantidad de anuncios concretos
     * @param $pagina int Página en la que nos encontramos
     * @param $busqueda array Contiene la búsqueda solicitada
     * @return array Los anuncios encontrados
     */
    public static function obtenerTodos($pagina, $busqueda)
    {
        $ELEMENTOS_POR_PAGINA = '2';
        $offset = ($pagina - 1) * $ELEMENTOS_POR_PAGINA;
        $queryBusqueda = self::devuelveBusqueda($busqueda);
        $BD = App::obtener('database');
        return $BD->selecionarTodoLimitado('anuncios', $ELEMENTOS_POR_PAGINA, $offset, $queryBusqueda);
    }

    /**
     * Consulta que devuelve los anuncios más recientes
     * @return array Los anuncios encontrados
     */
    public static function obtenerUltimos()
    {
        $cantidad = '3';
        $campo = 'fecha';
        $orden = 'DESC';
        $BD = App::obtener('database');
        return $BD->selecionarOrden('anuncios', $cantidad, $campo, $orden);
    }

    /**
     * Genera el string con la búsqueda que agregar en la consulta
     * @param $busqueda array Búsqueda solicitada
     * @return string Búsqueda adaptada a SQL
     */
    private static function devuelveBusqueda($busqueda)
    {
        $query = "";

        if(isset($busqueda['id']) && strlen($busqueda['id']) > 0){
            return "usuario='{$busqueda['id']}'";
        }

        if(isset($busqueda['titulo']) && strlen($busqueda['titulo']) > 0) {
            $query .= "titulo LIKE '%{$busqueda['titulo']}%' AND ";
        }

        if(isset($busqueda['tipo']) && strlen($busqueda['tipo']) > 0){
            if(isset($busqueda['precio']) && strlen($busqueda['precio']) > 0 && $busqueda['precio'] != '0') {
                $precio = number_format($busqueda['precio'],2,'.', '');
                $query .= "precioVenta {$busqueda['tipo']} '{$precio}' AND ";
            }
        }

        if(isset($busqueda['categoria']) && strlen($busqueda['categoria']) > 0) {
            $query .= "categoria REGEXP '{$busqueda['categoria']}' AND ";
        }

        if(isset($busqueda['estado']) && strlen($busqueda['estado']) > 0) {
            $query .= "estado REGEXP '{$busqueda['estado']}' ";
        }

        return preg_replace('/AND $/', '', $query);
    }

    /**
     * Consulta que devuelve un anuncio específico
     * @param $id int ID del anuncio
     * @return array Anuncio encontrado
     */
    public static function obtener($id)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('anuncios', "id={$id}");
    }

    /**
     * Consulta que devuelve todos los anuncios
     * @return array Anuncios encontrados
     */
    public static function listaAnuncios()
    {
        $BD = App::obtener('database');
        return $BD->selecionarTodo('anuncios');
    }

    /**
     * Consulta que devuelve las fotos que tiene un anuncio
     * @param $id int ID del anuncio
     * @return array Nombre de los ficheros de las fotos encontradas
     */
    public static function obtenerFotos($id)
    {
        $BD = App::obtener('database');
        $arr = $BD->seleccionar('fotos', "anuncio={$id}");
        $fotos = [];
        foreach ($arr as $foto){
            $fotos[] = $foto->fichero;
        }
        return $fotos;
    }

    /**
     * Consulta que agrega un nuevo anuncio
     * @param $datos array Datos del anuncio
     * @return void
     */
    public static function insertar($datos)
    {
        $BD = App::obtener('database');
        $BD->insertar('anuncios', $datos);
    }

    /**
     * Consulta que agrega una foto
     * @param $foto string Nombre del fichero de la foto
     * @param int $idAnuncio ID del anuncio
     * @return void
     */
    public static function insertaFotos($foto, $idAnuncio=false)
    {
        $pdo = \Conexion::realizar(App::obtener('config')['database']);
        if($idAnuncio == false) {
            $idQuery = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'anuncios' AND table_schema = DATABASE()";
            $consulta = $pdo->prepare($idQuery);
            $consulta->execute();
            $id = $consulta->fetchColumn();
        } else {
            $id = $idAnuncio;
        }
        $insertaQuery = "insert into fotos (fichero, anuncio) values ('{$foto}','{$id}')";
        $consulta = $pdo->prepare($insertaQuery);
        $consulta->execute();
    }

    /**
     * Consulta que llama al procedimiento de la tabla Logs
     * @param $anuncio int ID del anuncio
     * @param $tipo string Tipo de cambio a registrar (ACCESO, BAJA, etc...)
     * @return void
     */
    public static function llamaProcedimiento($anuncio, $tipo)
    {
        $pdo = \Conexion::realizar(App::obtener('config')['database']);
        $insertaQuery = "";
        if($anuncio == null) {
            $idQuery = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'anuncios' AND table_schema = DATABASE()";
            $consulta = $pdo->prepare($idQuery);
            $consulta->execute();
            $id = $consulta->fetchColumn();
            $insertaQuery = "CALL registraLog('{$id}', '{$tipo}', 'anuncios');";
        } else {
            $insertaQuery = "CALL registraLog('{$anuncio}', '{$tipo}', 'anuncios');";
        }
        $consulta = $pdo->prepare($insertaQuery);
        $consulta->execute();
    }

    /**
     * Consulta que elimina un anuncio
     * @param $id int ID del anuncio
     * @return void
     */
    public static function eliminar($id)
    {
        $BD = App::obtener('database');
        $BD->eliminar('anuncios', "id={$id}");
    }

    /**
     * Consulta que actualiza un anuncio
     * @param $id int ID del anuncio
     * @param $datos array Datos a actualizar
     */
    public static function actualizar($id, $datos)
    {
        $BD = App::obtener('database');
        $datoString = '';
        foreach ($datos as $key => $val) {
            $datoString .= "{$key}='${val}',";
        }
        $datoString = trim($datoString, ',');
        $BD->actualizar('anuncios', "id={$id}",$datoString);
    }

    /**
     * Consulta que devuelve una categoria
     * @param $codigo int Código de la categoria
     * @return array Categoria encontrada
     */
    public static function devuelveCategoria($codigo)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('categorias', "cod='{$codigo}'");
    }

    /**
     * Consulta que devuelve los datos del usuario
     * @param $codigo int ID del usuario
     * @return array Datos del usuario encontrado
     */
    public static function devuelveAutor($codigo)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('usuarios', "id='{$codigo}'");
    }

    /**
     * Consulta que devuelve todas las categorias
     * @return array Categorias encontradas
     */
    public static function listaCategorias()
    {
        $BD = App::obtener('database');
        return $BD->selecionarTodo('categorias');
    }

    /**
     * Consulta que devuelve un estado
     * @param $cod int Código del estado
     * @return array Datos encontrados
     */
    public static function devuelveEstado($cod)
    {
        $estados = ['Nuevo', 'Usado', 'Con defectos'];
        return $estados[$cod];
    }

    /**
     * Consulta que devuelve todos los estados
     * @return array Estados encontrados
     */
    public static function listaEstados()
    {
        return ['Nuevo', 'Usado', 'Con defectos'];
    }

    /**
     * Transforma el precio y porcentaje de un anuncio al formato solicitado por la BBDD
     * @param $datos array El anuncio
     * @return array Anuncio con los datos normalizados
     */
    public static function normalizaNumeros($datos)
    {
        $anuncio = $datos;
        if(!empty($anuncio['precio'])) {
            $anuncio['precio'] = number_format($anuncio['precio'],2,'.', '');
        }
        if(!empty($anuncio['porcentaje'])) {
            $anuncio['porcentaje'] = number_format($anuncio['porcentaje'],2,'.', '');
        }
        return $anuncio;
    }
}