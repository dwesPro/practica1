<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 02/12/2017
 * Hora: 22:52
 */

namespace App\Models;


class Validador
{
    /**
     * Comprueba si los datos de un usuario son válidos
     * @param $usuario array Datos a comprobar
     * @return array Errores encontrados
     */
    public static function validarUsuario($usuario)
    {
        $errores = [];
        if(empty($usuario['nif']) || strlen($usuario['nif']) > 9 || !preg_match('/^\d{8}(?![IÑOU])[A-Z]$/',$usuario['nif'])){
            $errores['nif'] = 'El NIF introducido es inválido.';
        }

        if (empty($usuario['nombre']) || strlen($usuario['nombre']) > 20) {
            $errores['nombre'] = 'El nombre introducido es inválido.';
        }

        if (empty($usuario['apellido1']) || strlen($usuario['apellido1']) > 15) {
            $errores['apellido1'] = 'El primer apellido es inválido.';
        }

        if (empty($usuario['apellido2']) || strlen($usuario['apellido2']) > 15) {
            $errores['apellido2'] = 'El segundo apellido es inválido.';
        }

        if(!empty($usuario['foto'])) {
//          TODO comprobar el ancho y alto, el tipo de archivo, etc, en el caso de las fotos
        }


        if (empty($usuario['usuario']) || strlen($usuario['usuario']) > 15) {
            $errores['usuario'] = 'El usuario introducido es inválido.';
        }

        if (empty($usuario['passwd']) || strlen($usuario['passwd']) > 30) {
            $errores['passwd'] = 'La contraseña introducida es inválida.';
        }

        if (empty($usuario['direccion']) || strlen($usuario['direccion']) > 50) {
            $errores['direccion'] = 'La dirección introducida es inválida.';
        }

        if (empty($usuario['poblacion']) || strlen($usuario['poblacion']) > 30) {
            $errores['poblacion'] = 'La población introducida es inválida.';
        }

        if (empty($usuario['codigop']) || strlen($usuario['codigop']) != 5 || !preg_match('/^[0-5]\d{4}$/',$usuario['codigop'])) {
            $errores['codigop'] = 'El codigo postal introducido es inválido.';
        }

        if (empty($usuario['provincia']) || !is_numeric($usuario['provincia'])) {
            $errores['provincia'] = 'La provincia es obligatoria.';
        }

        if (empty($usuario['telf']) || strlen($usuario['telf']) != 9 || !preg_match('/^[6789]\d{8}$/',$usuario['telf'])) {
            $errores['telf'] = 'El teléfono introducido es invalido.';
        }

        if (empty($usuario['correo']) || strlen($usuario['correo']) > 45 || !preg_match('/^[\w\.-]+\@[\w\.-]+\.[a-z\.]{2,6}$/',$usuario['correo'])) {
            $errores['correo'] = 'El correo introducido es invalido.';
        }

        if (empty($usuario['web']) || strlen($usuario['web']) > 40 || !preg_match('/^(((http|https):\/\/)|(\/)|(..\/))(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/',$usuario['web'])) {
            $errores['web'] = 'La web introducida es invalida.';
        }

        if (!empty($usuario['blog'])) {
            if(strlen($usuario['blog']) > 40 || !preg_match('/^(http|https):\/\/(www\.)?\w+-?\w+\.(wordpress|blogspot).\w{2,3}(\.\w{2,3})?$/',$usuario['blog'])) {
                $errores['blog'] = 'El blog introducido es invalido.';
            }
        }

        if (!empty($usuario['twitter'])) {
            if(strlen($usuario['twitter']) > 40 || !preg_match('/^(http|https):\/\/(www\.)?twitter\.com\/\w+$/',$usuario['twitter'])) {
                $errores['twitter'] = 'La cuenta de Twitter introducida es invalida.';
            }
        }

        if (!isset($usuario['admin']) || !is_numeric($usuario['admin'])) {
            $errores['admin'] = 'Debe indicar el tipo de usuario.';
        }

        return $errores;
    }

    /**
     * Comprueba si los datos de un anuncio son válidos
     * @param $anuncio array Datos a comprobar
     * @return array Errores encontrados
     */
    public static function validarAnuncio($anuncio)
    {
        $errores = [];
        if(empty($anuncio['titulo']) || strlen($anuncio['titulo']) > 50) {
            $errores['titulo'] = 'Titulo introducido inválido';
        }

        if(empty($anuncio['precio']) || strlen($anuncio['precio']) > 7 || !is_numeric($anuncio['precio'])) {
            $errores['precio'] = 'Precio introducido inválido';
        }

        if(empty($anuncio['porcentaje']) || strlen($anuncio['porcentaje']) > 6 || !is_numeric($anuncio['porcentaje'])) {
            $errores['porcentaje'] = 'Porcentaje introducido inválido';
        }

        if(!isset($anuncio['estado']) || strlen($anuncio['estado']) > 1) {
            $errores['estado'] = 'El campo estado es obligatorio';
        }

        if(empty($anuncio['categoria']) || strlen($anuncio['categoria']) > 2 || !is_numeric($anuncio['categoria'])) {
            $errores['categoria'] = 'El campo categoria es obligatorio';
        }

        if(empty($anuncio['datos']) || strlen($anuncio['datos']) > 200) {
            $errores['datos'] = 'Debe introducir una descripción de producto';
        }

        if(!$anuncio['imagen']) {
            //TODO: Comprobar anchura, altura y tipo de archivo.
            $errores['imagen'] = 'Debe introducir alguna imagen del producto';
        }

        return $errores;
    }
}