<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 23:40
 */

namespace App\Models;
use \App\Core\App;

class Usuario
{
    /**
     * Consulta que devuelve una cantidad de usuarios concretos
     * @param $pagina int Página en la que nos encontramos
     * @param $busqueda array Contiene la búsqueda solicitada
     * @return array Los usuarios encontrados
     */
    public static function obtenerTodos($pagina, $busqueda)
    {
        $ELEMENTOS_POR_PAGINA = '4';
        $offset = ($pagina - 1) * $ELEMENTOS_POR_PAGINA;
        $queryBusqueda = self::devuelveBusqueda($busqueda);
        $BD = App::obtener('database');
        return $BD->selecionarTodoLimitado('usuarios', $ELEMENTOS_POR_PAGINA, $offset, $queryBusqueda);
    }

    /**
     * Consulta que llama al procedimiento de la tabla Logs
     * @param $usuario int ID del usuario
     * @param $tipo string Tipo de cambio a registrar (ACCESO, BAJA, etc...)
     * @return void
     */
    public static function llamaProcedimiento($usuario, $tipo)
    {
        $pdo = \Conexion::realizar(App::obtener('config')['database']);
        $insertaQuery = "";
        if($usuario == null) {
            $idQuery = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'usuarios' AND table_schema = DATABASE()";
            $consulta = $pdo->prepare($idQuery);
            $consulta->execute();
            $id = $consulta->fetchColumn();
            $insertaQuery = "CALL registraLog('{$id}', '{$tipo}', 'usuarios');";
        } else {
            $insertaQuery = "CALL registraLog('{$usuario}', '{$tipo}', 'usuarios');";
        }
        $consulta = $pdo->prepare($insertaQuery);
        $consulta->execute();
    }

    /**
     * Genera el string con la búsqueda que agregar en la consulta
     * @param $busqueda array Búsqueda solicitada
     * @return string Búsqueda adaptada a SQL
     */
    private static function devuelveBusqueda($busqueda)
    {
        $query = "";
        if(isset($busqueda['usuario']) && strlen($busqueda['usuario']) > 0) {
            $query .= "usuario LIKE '%{$busqueda['usuario']}%' AND ";
        }

        if(isset($busqueda['rol']) && strlen($busqueda['rol']) > 0){
            $query .= "admin REGEXP '{$busqueda['rol']}' AND ";
        }

        if(isset($busqueda['email']) && strlen($busqueda['email']) > 0) {
            $query .= "correo LIKE '%{$busqueda['email']}%' ";
        }

        return preg_replace('/AND $/', '', $query);
    }

    /**
     * Consulta que devuelve los datos de un usuario
     * @param $id int ID del usuario
     * @return array Datos del usuario encontrado
     */
    public static function obtener($id)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('usuarios', "id={$id}");
    }

    /**
     * Consulta que devuelve todos los usuarios
     * @return array Datos encontrados
     */
    public static function listadoUsuarios()
    {
        $BD = App::obtener('database');
        return $BD->selecionarTodo('usuarios');
    }

    /**
     * Consulta que agrega un nuevo usuario
     * @param $datos array Datos del usuario
     * @return void
     */
    public static function insertar($datos)
    {
        $BD = App::obtener('database');
        $BD->insertar('usuarios', $datos);
    }

    /**
     * Consulta que elimina un usuario
     * @param $id int ID del usuario
     * @return void
     */
    public static function eliminar($id)
    {
        $BD = App::obtener('database');
        $BD->eliminar('anuncios', "usuario={$id}");
        $BD->eliminar('usuarios', "id={$id}");
    }

    /**
     * Consulta que actualiza los datos de un usuario
     * @param $id int ID del usuario
     * @param $datos array Datos a actualizar
     * @return void
     */
    public static function actualizar($id, $datos)
    {
        $BD = App::obtener('database');
        $datoString = '';
        foreach ($datos as $key => $val) {
            $datoString .= "{$key}='${val}',";
        }
        $datoString = trim($datoString, ',');
        $BD->actualizar('usuarios', "id={$id}",$datoString);
    }

    /**
     * Consulta que devuelve una provincia
     * @param $codigo int Código de la provincia
     * @return array Datos encontrados
     */
    public static function devuelveProvincia($codigo)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('provincias', "cod='{$codigo}'");
    }

    /**
     * Consulta que devuelve todas las provincia
     * @return array Datos encontrados
     */
    public static function listaProvincias()
    {
        $BD = App::obtener('database');
        return $BD->selecionarTodo('provincias');
    }

    /**
     * Consulta que comprueba si un usuario existe
     * @param $usuario string Nombre del usuario
     * @param $passwd string Contraseña del usuario
     * @return array Usuario encontrado
     */
    public static function compruebaUser($usuario, $passwd)
    {
        $BD = App::obtener('database');
        return $BD->seleccionar('usuarios', "usuario='{$usuario}' AND passwd='{$passwd}'");
    }

    /**
     * Consulta que comprueba si un usuario ya existe
     * @param $usuario string Nombre del usuario
     * @param $nif string NIF del usuario
     * @return bool Resultado de la consulta
     */
    public static function usuarioRepetido($usuario, $nif)
    {
        $BD = App::obtener('database');
        $arr = $BD->seleccionar('usuarios', "usuario='{$usuario}'");
        $arr2 = $BD->seleccionar('usuarios', "nif='{$nif}'");
        return (count($arr) > 0 || count($arr2) > 0);
    }

    public static function obtieneLogs()
    {
        $BD = App::obtener('database');
        return $BD->selecionarTodo('logs');
    }
}