<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $titulo ?></title>
    <style>
        table {
            border-collapse: collapse;
            margin-bottom: 10px;
        }

        table, td, th, tr {
            border: 1px solid black;
        }

        td, th {
            padding: 3px;
        }

        td {
            display: block;
            width: 350px;
            word-wrap:break-word;
        }
    </style>
</head>
<body>
<?php foreach ($datos as $key => $dato) : ?>
    <table>
        <?php foreach ($datos[$key] as $key=>$valor) : ?>
        <tr>
            <th><?= $key ?></th>
            <td><?= $valor ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endforeach; ?>
</body>
</html>