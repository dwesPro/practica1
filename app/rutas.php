<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 2:45
 */

$router->get('', 'ControladorPaginas@inicio');
$router->get('usuarios', 'ControladorUsuarios@index');
$router->post('usuarios', 'ControladorUsuarios@index');
$router->get('anuncios', 'ControladorAnuncios@index');
$router->post('anuncios', 'ControladorAnuncios@index');
$router->get('login', 'ControladorPaginas@login');
$router->post('login', 'ControladorPaginas@login');
$router->get('logout', 'ControladorPaginas@logout');
$router->get('registro', 'ControladorPaginas@registro');
$router->get('pdfUsuarios', 'ControladorPaginas@pdfUsuarios');
$router->get('pdfAnuncios', 'ControladorPaginas@pdfAnuncios');
$router->get('logs', 'ControladorPaginas@logsReg');

$router->get('usuarios/agregaUsuario', 'ControladorUsuarios@guardar');
$router->post('usuarios/agregaUsuario', 'ControladorUsuarios@guardar');
$router->get('usuarios/verUsuario', 'ControladorUsuarios@ver');
$router->post('usuarios/modificarUsuario', 'ControladorUsuarios@modificar');
$router->post('usuarios/eliminarUsuario', 'ControladorUsuarios@eliminar');

$router->get('anuncios/agregaAnuncio', 'ControladorAnuncios@guardar');
$router->post('anuncios/agregaAnuncio', 'ControladorAnuncios@guardar');
$router->get('anuncios/verAnuncio', 'ControladorAnuncios@ver');
$router->post('anuncios/modificarAnuncio', 'ControladorAnuncios@modificar');
$router->post('anuncios/eliminarAnuncio', 'ControladorAnuncios@eliminar');