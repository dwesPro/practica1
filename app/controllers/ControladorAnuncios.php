<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 26/11/2017
 * Hora: 18:08
 */

namespace App\Controllers;

use App\Models\Anuncio;
use App\Models\Validador;


class ControladorAnuncios
{
    /**
     * Muestra la lista de anuncios
     * @return void
     */
    public function index()
    {
        $pagina = '1';
        if(isset($_GET['p'])) {
            $pagina = $_GET['p'];
        }
        $busqueda = [];
        $sesionBusqueda = false;
        if(isset($_GET['u'])){
            $busqueda['id'] = $_GET['u'];
            $_SESSION['busquedaAnun'] = serialize($busqueda);
        }
        if(isset($_POST['busqueda']) && !isset($_GET['u'])) {
            $busqueda = $_POST['busqueda'];
            $_SESSION['busquedaAnun'] = serialize($_POST['busqueda']);
        }
        if(isset($_POST['cancelar'])){
            unset($_SESSION['busquedaAnun']);
        }
        if(isset($_SESSION['busquedaAnun'])) {
            $busqueda = unserialize($_SESSION['busquedaAnun']);
            $sesionBusqueda = true;
        }
        $arr = Anuncio::obtenerTodos($pagina, $busqueda);
        $anuncios = $arr['datos'];
        $paginacion = [
            'actual' => $pagina,
            'total' => $arr['total']
        ];
        foreach ($anuncios as $key => $anuncio)
        {
            $anuncios[$key]->categoria = Anuncio::devuelveCategoria($anuncio->categoria)[0]->nombre;
            $anuncios[$key]->usuarioNom = Anuncio::devuelveAutor($anuncio->usuario)[0]->usuario;
            $anuncios[$key]->estado = Anuncio::devuelveEstado($anuncio->estado);
            $anuncios[$key]->imagen = Anuncio::obtenerFotos($anuncio->id);
        }
        $categorias = Anuncio::listaCategorias();
        $estados = Anuncio::listaEstados();
        if(isset($_SESSION['usuario'])){
            return view('anuncios', ['anuncios' => $anuncios, 'sesionBusqueda' => $sesionBusqueda,'usuario' => $_SESSION['usuario'], 'paginacion' => $paginacion, 'categorias' => $categorias, 'estados' => $estados]);
        } else {
            return view('anuncios', ['anuncios' => $anuncios, 'sesionBusqueda' => $sesionBusqueda,'paginacion' => $paginacion, 'categorias' => $categorias, 'estados' => $estados]);
        }
    }

    /**
     * Guarda un anuncio
     * @return void
     */
    public function guardar()
    {
        if(isset($_SESSION['usuario'])) {
            $categorias = Anuncio::listaCategorias();
            $estados = Anuncio::listaEstados();
            if (isset($_POST['anuncio'])) {
                $anuncio = $_POST['anuncio'];
                $anuncio = Anuncio::normalizaNumeros($anuncio);
                $anuncio['imagen'] = false;
                if(!empty($_FILES['fotos']['name'][0])){
                    $anuncio['imagen'] = true;
                }
                $errores = Validador::validarAnuncio($anuncio);
                unset($anuncio['imagen']);
                if(count($errores) > 0){
                    return view('nuevoAnuncio', ['categorias' => $categorias, 'estados' => $estados, 'errores' => $errores, 'anuncio' => $_POST['anuncio']]);
                } else {
                    $anuncio['usuario'] = $_SESSION['usuario']['id'];
                    $anuncio = Anuncio::normalizaNumeros($anuncio);
                    if(!empty($_FILES['fotos']['name'][0])){
                        $dir_subida= './public/images/fotos/';
                        $cantidad = count($_FILES['fotos']['name']);
                        for ($i = 0; $i < $cantidad; $i++){
                            $dir_temporal = $_FILES['fotos']['tmp_name'][$i];
                            $extension = pathinfo(basename($_FILES['fotos']['name'][$i]), PATHINFO_EXTENSION);
                            $nombreFichero = basename($_FILES['fotos']['tmp_name'][$i]) . ".{$extension}";
                            $dir_fichero = $dir_subida . $nombreFichero;
                            if(move_uploaded_file($dir_temporal, $dir_fichero)) {
                                Anuncio::insertaFotos($nombreFichero);
                            }
                        }
                    }
                    Anuncio::insertar($anuncio);
                    Anuncio::llamaProcedimiento($_SESSION['usuario']['id'], 'NUEVO');
                    return redirigir('anuncios');
                }
            } else {
                return view('nuevoAnuncio', ['categorias' => $categorias, 'estados' => $estados]);
            }
        } else {
            redirigir('');
        }
    }

    /**
     * Elimina un anuncio
     * @return void
     */
    public function eliminar()
    {
        if(isset($_SESSION['usuario'])){
            $anuncio = Anuncio::obtener($_POST['id'])[0];
            if ($_SESSION['usuario']['id'] == $anuncio->usuario || $_SESSION['usuario']['rol'] == "1") {
                if(isset($_POST['solicitud'])){
                    $datos = [
                        "id" => $_POST['id'],
                        "tipo" => "anuncio",
                        "titulo" => $anuncio->titulo,
                        "controlador" => "eliminarAnuncio"
                    ];
                    view('confirmacion', $datos);
                } else {
                    Anuncio::eliminar($_POST['id']);
                    Anuncio::llamaProcedimiento($_SESSION['usuario']['id'], 'ELIMINADO');
                    return redirigir('anuncios');
                }
            } else {
                redirigir('');
            }
        } else {
            redirigir('');
        }
    }

    /**
     * Muestra un anuncio concreto
     * @return void
     */
    public function ver()
    {
        $anuncio = Anuncio::obtener($_GET['id']);
        $anuncio[0]->categoria = Anuncio::devuelveCategoria($anuncio[0]->categoria)[0]->nombre;
        $anuncio[0]->estado = Anuncio::devuelveEstado($anuncio[0]->estado);
        $anuncio[0]->usuario = Anuncio::devuelveAutor($anuncio[0]->usuario)[0]->usuario;
        $fotos = Anuncio::obtenerFotos($_GET['id']);
        return view('verAnuncio', ['anuncio' => $anuncio[0], 'fotos' => $fotos]);
    }

    /**
     * Modifica un anuncio concreto
     * @return void
     */
    public function modificar()
    {
        if(isset($_SESSION['usuario'])){
            $categorias = Anuncio::listaCategorias();
            $estados = Anuncio::listaEstados();
            if(isset($_POST['id'])) {
                $anuncio = Anuncio::obtener($_POST['id'])[0];
                if ($_SESSION['usuario']['id'] == $anuncio->usuario || $_SESSION['usuario']['rol'] == "1") {
                    return view('modificarAnuncio', ['anuncio' => $anuncio, 'categorias' => $categorias, 'estados' => $estados]);
                } else {
                    redirigir('');
                }
            } else {
                $arr = Anuncio::obtener($_POST['anuncio']['id'])[0];
                $anuncio = array_merge((array)$arr,$_POST['anuncio']);
                $anuncio = Anuncio::normalizaNumeros($anuncio);
                $anuncio['imagen'] = true;
                $errores = Validador::validarAnuncio($anuncio);
                if(count($errores) > 0) {
                    return view('modificarAnuncio', ['anuncio' => $anuncio, 'categorias' => $categorias, 'estados' => $estados, 'errores' => $errores]);
                } else {
                    if(isset($_FILES['fotos'])) {
                        $dir_subida= './public/images/fotos/';
                        $cantidad = count($_FILES['fotos']['name']);
                        for ($i = 0; $i < $cantidad; $i++){
                            $dir_temporal = $_FILES['fotos']['tmp_name'][$i];
                            $extension = pathinfo(basename($_FILES['fotos']['name'][$i]), PATHINFO_EXTENSION);
                            $nombreFichero = basename($_FILES['fotos']['tmp_name'][$i]) . ".{$extension}";
                            $dir_fichero = $dir_subida . $nombreFichero;
                            if(move_uploaded_file($dir_temporal, $dir_fichero)) {
                                Anuncio::insertaFotos($nombreFichero, $_POST['anuncio']['id']);
                            }
                        }
                    }
                    Anuncio::actualizar($anuncio['id'], $_POST['anuncio']);
                    Anuncio::llamaProcedimiento($_SESSION['usuario']['id'], 'MODIFICADO');
                    return redirigir('anuncios');
                }
            }
        } else {
            redirigir('');
        }
    }
}