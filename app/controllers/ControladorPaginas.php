<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 17:47
 */

namespace App\Controllers;

use App\Models\Usuario;
use App\Models\Anuncio;

class ControladorPaginas
{
    /**
     * Muestra la página principal
     * @return void
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function inicio()
    {
        $anuncios = Anuncio::obtenerUltimos();
        foreach ($anuncios as $key => $anuncio){
            $anuncios[$key]->imagen = Anuncio::obtenerFotos($anuncio->id);
            $anuncios[$key]->categoria = Anuncio::devuelveCategoria($anuncio->categoria)[0]->nombre;
            $anuncios[$key]->usuarioNom = Anuncio::devuelveAutor($anuncio->usuario)[0]->usuario;
            $anuncios[$key]->estado = Anuncio::devuelveEstado($anuncio->estado);
        }
        return view('index', ['anuncios' => $anuncios]);
    }

    /**
     * Muestra la página de registro
     * @return void
     */
    public function registro()
    {
        redirigir('usuarios/agregaUsuario');
    }

    /**
     * Muestra la vista de anuncios
     * @return void
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function anuncios()
    {
        return view('anuncios');
    }

    /**
     * Muestra la página de login
     * @return void
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function login()
    {
        if(isset($_SESSION['usuario'])) {
            redirigir('');
        }
        if(isset($_POST['usuario'])) {
            $datos = $_POST['usuario'];
            $usuario = Usuario::compruebaUser($datos['usuario'], $datos['passwd']);
            if(count($usuario) > 0) {
                $usuario = $usuario[0];
                $_SESSION['usuario'] = [
                  'id' => $usuario->id,
                  'usuario' => $usuario->usuario,
                  'rol' => $usuario->admin,
                  'hora' => date('H:i')
                ];
                Usuario::llamaProcedimiento($_SESSION['usuario']['id'], 'LOGIN');
                if($datos['recuerdaPass']) {
                    $datoCookie = [
                        'usuario' => $usuario->usuario,
                        'passwd' => $usuario->passwd
                    ];
                    setcookie('datos', serialize($datoCookie), time() + 60 * 60 * 24 * 120);
                } else {
                    setcookie('datos', '', time()-7000000);
                }
                if($datos['recuerda']) {
                    setcookie('sesion', serialize($_SESSION['usuario']), time() + 60 * 60 * 24 * 120);
                }
                redirigir('');
            } else {
                $error = true;
                return view('login', ['error' => $error]);
            }
        } else {
            if (isset($_COOKIE['datos'])) {
                return view('login', ['datos' => unserialize($_COOKIE['datos'])]);
            } else {
                return view('login');
            }
        }
    }

    /**
     * Permite cerrar sesión
     * @return void
     */
    public function logout()
    {
        if(!isset($_SESSION['usuario'])) {
            redirigir('');
        }
        Usuario::llamaProcedimiento($_SESSION['usuario']['id'], 'LOGOUT');
        unset($_SESSION['usuario']);
        setcookie('sesion', '', time()-7000000);
        redirigir('');
    }

    /**
     * Genera un PDF con la lista de usuarios
     * @return void;
     */
    public function pdfUsuarios()
    {
        if(isset($_SESSION['usuario']) && $_SESSION['usuario']['rol'] == "1") {
            $usuarios = Usuario::listadoUsuarios();
            foreach ($usuarios as $key => $usuario)
            {
                $usuarios[$key]->admin = $usuarios[$key]->admin == 0 ? 'Anunciante' : 'Administrador';
            }
            escribePDF($usuarios, 'Listado de usuarios', 'listaUsuarios');
        } else {
            redirigir('');
        }
    }

    /**
     * Genera un PDF con la lista de anuncios
     * @return void
     */
    public function pdfAnuncios()
    {
        if(isset($_SESSION['usuario']) && $_SESSION['usuario']['rol'] == "1") {
            $anuncios = Anuncio::listaAnuncios();
            foreach ($anuncios as $key => $anuncio)
            {
                $anuncios[$key]->categoria = Anuncio::devuelveCategoria($anuncio->categoria)[0]->nombre;
                $anuncios[$key]->usuario = Anuncio::devuelveAutor($anuncio->usuario)[0]->usuario;
                $anuncios[$key]->estado = Anuncio::devuelveEstado($anuncio->estado);
            }
            escribePDF($anuncios, 'Listado de anuncios', 'listaAnuncios');
        } else {
            redirigir('');
        }
    }

    public function logsReg()
    {
        if(isset($_SESSION['usuario']) && $_SESSION['usuario']['rol'] == "1") {
            $logs = Usuario::obtieneLogs();
            return view("logs", ['logs' => $logs]);
        } else {
            redirigir('');
        }
    }
}