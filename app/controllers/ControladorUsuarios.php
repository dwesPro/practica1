<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 18:34
 */

namespace App\Controllers;

use App\Models\Usuario;
use App\Models\Validador;

class ControladorUsuarios
{
    /**
     * Muestra la lista de usuarios
     * @return void
     */
    public function index()
    {
        $pagina = '1';
        if(isset($_GET['p'])) {
            $pagina = $_GET['p'];
        }
        $busqueda = [];
        $sesionBusqueda = false;
        if(isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $_SESSION['busquedaUser'] = serialize($_POST['busqueda']);
        }
        if(isset($_POST['cancelar'])){
            unset($_SESSION['busquedaUser']);
        }
        if(isset($_SESSION['busquedaUser'])) {
            $busqueda = unserialize($_SESSION['busquedaUser']);
            $sesionBusqueda = true;
        }
        $arr = Usuario::obtenerTodos($pagina, $busqueda);
        $usuarios = $arr['datos'];
        $paginacion = [
            'actual' => $pagina,
            'total' => $arr['total']
        ];
        if(isset($_SESSION['usuario']) && $_SESSION['usuario']['rol'] == "1") {
            return view('usuarios', ['usuarios' => $usuarios, 'paginacion' => $paginacion, 'sesionBusqueda' => $sesionBusqueda]);
        } else {
            redirigir('');
        }
    }

    /**
     * Permite agregar un nuevo usuario
     * @return void
     */
    public function guardar()
    {
        if((isset($_SESSION['usuario']) && $_SESSION['usuario']['rol'] == "1") || !isset($_SESSION['usuario'])) {
            $provincias = Usuario::listaProvincias();
            if(isset($_POST['usuario'])) {
                if(!isset($_SESSION['usuario'])){
                    $_POST['usuario']['admin'] = '0';
                }
                $errores = Validador::validarUsuario($_POST['usuario']);
                if(Usuario::usuarioRepetido($_POST['usuario']['usuario'], $_POST['usuario']['nif'])){
                    $errores['repetido'] = 'El usuario introducido ya existe';
                }
                if(count($errores) > 0) {
                    return view('nuevoUsuario', ['provincias' => $provincias, 'errores' => $errores, 'usuario' => $_POST['usuario']]);
                } else {
                    if(!empty($_FILES['usuario']['name']['foto'])) {
                        $dir_subida= './public/images/avatares/';
                        $dir_temporal = $_FILES['usuario']['tmp_name']['foto'];
                        $extension = pathinfo(basename($_FILES['usuario']['name']['foto']), PATHINFO_EXTENSION);
                        $nombreFichero = basename($_FILES['usuario']['tmp_name']['foto']) . ".{$extension}";
                        $dir_fichero = $dir_subida . $nombreFichero;
                        if(move_uploaded_file($dir_temporal, $dir_fichero)) {
                            $_POST['usuario']['foto'] = $nombreFichero;
                        }
                    } else {
                        $_POST['usuario']['foto'] = 'usuario.jpg';
                    }
                    Usuario::insertar($_POST['usuario']);
                    Usuario::llamaProcedimiento(null, 'ALTA');
                    return redirigir('usuarios');
                }
            } else {
                return view('nuevoUsuario', ['provincias' => $provincias]);
            }
        } else {
            redirigir('');
        }
    }

    /**
     * Permite eliminar un usuario
     * @return void
     */
    public function eliminar()
    {
        if(isset($_SESSION['usuario']) && ($_SESSION['usuario']['rol'] == "1" || $_SESSION['usuario']['id'] == $_POST['id'])) {
            if(isset($_POST['solicitud'])){
                $titulo = Usuario::obtener($_POST['id'])[0]->usuario;
                $datos = [
                    "id" => $_POST['id'],
                    "tipo" => "usuario",
                    "titulo" => $titulo,
                    "controlador" => "eliminarUsuario"
                ];
                view('confirmacion', $datos);
            } else {
                Usuario::eliminar($_POST['id']);
                Usuario::llamaProcedimiento($_SESSION['usuario']['id'], 'BAJA');
                return redirigir('usuarios');
            }

        } else {
            redirigir('');
        }
    }

    /**
     * Permite ver un usuario concreto
     * @return void
     */
    public function ver()
    {
        if(isset($_SESSION['usuario']) && ($_SESSION['usuario']['rol'] == "1" || $_SESSION['usuario']['id'] == $_GET['id'])) {
            $usuario = Usuario::obtener($_GET['id']);
            $usuario[0]->provincia = Usuario::devuelveProvincia($usuario[0]->provincia)[0]->nombre;
            return view('verUsuario', ['usuario' => $usuario[0]]);
        } else {
            redirigir('');
        }
    }

    /**
     * Permite modificar un usuario concreto
     * @return void
     */
    public function modificar()
    {
        if(isset($_SESSION['usuario']) && ($_SESSION['usuario']['rol'] == "1" || $_SESSION['usuario']['id'] == $_POST['id'])) {
            $provincias = Usuario::listaProvincias();
            if(isset($_POST['id'])) {
                $usuario = Usuario::obtener($_POST['id']);
                return view('modificarUsuario', ['usuario' => $usuario[0], 'provincias' => $provincias]);
            } else {
                $arr = Usuario::obtener($_POST['usuario']['id'])[0];
                $usuario = array_merge((array)$arr,$_POST['usuario']);
                $errores = Validador::validarUsuario($usuario);
                if(count($errores) > 0) {
                    return view('modificarUsuario', ['provincias' => $provincias, 'errores' => $errores, 'usuario' => $usuario]);
                } else {
                    if(!empty($_FILES['usuario']['name']['foto'])) {
                        $dir_subida= './public/images/avatares/';
                        $dir_temporal = $_FILES['usuario']['tmp_name']['foto'];
                        $extension = pathinfo(basename($_FILES['usuario']['name']['foto']), PATHINFO_EXTENSION);
                        $nombreFichero = basename($_FILES['usuario']['tmp_name']['foto']) . ".{$extension}";
                        $dir_fichero = $dir_subida . $nombreFichero;
                        if(move_uploaded_file($dir_temporal, $dir_fichero)) {
                            $_POST['usuario']['foto'] = $nombreFichero;
                        }
                    }
                    Usuario::actualizar($usuario['id'], $_POST['usuario']);
                    Usuario::llamaProcedimiento($_SESSION['usuario']['id'], 'ACTUALIZACIÓN');
                    return redirigir('usuarios');
                }
            }
        } else {
            redirigir('');
        }
    }
}