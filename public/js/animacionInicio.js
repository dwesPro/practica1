// Separamos cada letra en un div
$('.contAnimacion .letters').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop: false})
    .add({
        targets: '.contAnimacion .letter',
        scale: [0, 1],
        duration: 1500,
        elasticity: 600,
        delay: function(el, i) {
            return 45 * (i+1)
        }
    });