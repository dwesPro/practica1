$(document).ready(function () {
    $("#fImagen").fileinput({
        theme: "fa",
        language: "es",
        overwriteInitial: true,
        allowedFileExtensions: ["jpg", "png"],
        showClose: false
    });
});