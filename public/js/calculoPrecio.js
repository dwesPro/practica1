function asignaResultado(precio, porcentaje) {
    if(precio.length === 0|| porcentaje.length === 0){
        return;
    }
    precio = parseFloat(precio);
    porcentaje = parseFloat(porcentaje);
    $(fPVenta).val((precio + (precio * porcentaje * 0.01)).toFixed(2));
}

$(document).ready(function () {
    asignaResultado($("#fPrecio").val(), $("#fPorc").val());

    $("#fPrecio").on('focusout', function () {
       var valor = $(this).val();
        asignaResultado(valor, $("#fPorc").val());
    });

    $("#fPorc").on('focusout', function () {
        var valor = $(this).val();
        asignaResultado($("#fPrecio").val(), valor);
    });
});