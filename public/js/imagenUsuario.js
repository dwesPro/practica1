$(document).ready(function () {
    $("#fichero").fileinput({
        theme: "fa",
        language: "es",
        overwriteInitial: true,
        allowedFileExtensions: ["jpg", "png"],
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fa fa-folder-open" aria-hidden="true"></i>\n',
        removeIcon: '<i class="fa fa-trash" aria-hidden="true"></i>\n',
        elErrorContainer: '#msgError',
        msgErrorClass: 'alert alert-danger mt-5 text-center',
        defaultPreviewContent: '<img src="../public/images/avatares/usuario.jpg">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'}
    });
});