<?php
/**
 * Realizado con PhpStorm.
 * Usuario: Alberto
 * Fecha: 25/11/2017
 * Hora: 1:14
 */

require 'vendor/autoload.php';
require 'core/arranque.php';

use App\Core\Router;
use App\Core\Solicitud;

session_start();
if(isset($_COOKIE['sesion'])) {
    $_SESSION['usuario'] = unserialize($_COOKIE['sesion']);
}

Router::cargar('app/rutas.php')
    ->dirigir(Solicitud::uri(), Solicitud::metodo());